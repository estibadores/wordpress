FROM registry.sindominio.net/nginx-php

RUN apt-get update && \
    apt-get -qy install --no-install-recommends \
        gnupg ca-certificates less curl jq unzip wget default-mysql-client \
        php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-redis && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install wp-cli
ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar /usr/local/bin/wp
RUN chmod +rx /usr/local/bin/wp

# verify wp-cli signature
COPY wp-key.asc /key.asc
ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar.asc /wp.asc
RUN gpg --import /key.asc && \
    gpg --verify /wp.asc /usr/local/bin/wp

RUN mkdir /app
WORKDIR /app

# install wordpress
RUN wp --allow-root core download
ADD wp-config.php /app/

# install themes
ADD themes /app/
RUN cd wp-content/themes && \
    for theme in `cat /app/themes`; do \
        DOWNLOAD_URL=`curl -s "https://api.wordpress.org/themes/info/1.1/?action=theme_information&request\[slug\]=$theme" |jq -r '.download_link'` && \
		wget -q -O theme.zip $DOWNLOAD_URL && \
		unzip -q theme.zip && \
		rm theme.zip; \
    done

# install plugins
ADD plugins /app/
RUN cd wp-content/plugins && \
    for plugin in `cat /app/plugins`; do \
        DOWNLOAD_URL=`curl -s "https://api.wordpress.org/plugins/info/1.0/$plugin.json" |jq -r '.download_link'` && \
	wget -q -O plugin.zip $DOWNLOAD_URL && \
	unzip -q plugin.zip && \
	rm plugin.zip; \
    done

# install languages (core, plugins, themes)
ADD languages /app/
RUN mkdir -p wp-content/languages wp-content/languages/plugins wp-content/languages/themes && \
	WP_VERSION=$(wp --allow-root core version) && \ 
	for language in `cat /app/languages`; do \
		cd /app/wp-content/languages && \
		echo "Install Language Files $language" && \ 
		DOWNLOAD_URL=`curl -s "https://api.wordpress.org/translations/core/1.0/?version=$WP_VERSION" | jq -r '.translations[] | select(.language=="'$language'").package '` && \
		[ -z $DOWNLOAD_URL ] && echo "Download language file for $language not exist" && \ 
		[ -z $DOWNLOAD_URL ] || wget -q -O language.zip $DOWNLOAD_URL && \
		[ -f language.zip ] && unzip -q language.zip && \
		[ -f language.zip ] && rm language.zip && \
		cd plugins && \ 
		for plugin in `cat /app/plugins`; do \
			DOWNLOAD_URL=`curl -s "https://api.wordpress.org/translations/plugins/1.0/?slug=$plugin" | jq -r '.translations[] | select(.language=="'$language'").package '` && \ 
			[ -z $DOWNLOAD_URL ] && echo "Download language file for $plugin : $language not exist" && \ 
			[ -z $DOWNLOAD_URL ] || wget -q -O language.zip $DOWNLOAD_URL && \
			[ -z $DOWNLOAD_URL ] || unzip -q language.zip && \
			[ -z $DOWNLOAD_URL ] ||  rm language.zip; \
		done && \
		cd ../themes && \ 
		for theme in `cat /app/themes`; do \
			DOWNLOAD_URL=`curl -s "https://api.wordpress.org/translations/themes/1.0/?slug=$theme" | jq -r '.translations[] | select(.language=="'$language'").package '` && \ 
			[ -z $DOWNLOAD_URL ] && echo "Download language file for $theme : $language not exist" && \ 
			[ -z $DOWNLOAD_URL ] || wget -q -O language.zip $DOWNLOAD_URL && \
			[ -z $DOWNLOAD_URL ] || unzip -q language.zip && \
			[ -z $DOWNLOAD_URL ] ||  rm language.zip; \
		done; \
	done

VOLUME /app/wp-content/uploads

ADD setup /etc/setup

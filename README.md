# container image of WordPress

```
$ mkdir uploads db
$ cp env.sample .env
$ vim .env
```

Customize parameters

## Custom themes or pluggins

To include custom themes or pluggins mount them directly into /app/wp-content/:
```
    volumes:
      - ./uploads:/app/wp-content/uploads
      - ./mytheme:/app/wp-content/themes/mytheme
```
